use clap::{arg, Parser};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    /// Config file path
    #[arg(short = 'c', long = "config-file")]
    pub config_file: Option<String>,
}
