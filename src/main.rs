pub mod config;

use clap::Parser;
use rbackup_core::config::Config;
use std::path::PathBuf;

use crate::config::{get_config, get_config_location};
use color_eyre::eyre::{Context, Result as EyreResult};
use rbackup_cli::command::Cli;

fn main() -> EyreResult<()> {
    color_eyre::install()?;

    let cli = Cli::parse();
    let config_location = cli
        .config_file
        .map_or_else(get_config_location, PathBuf::from);
    let config: Config = get_config(&config_location)?;
    config
        .start_backup()
        .wrap_err("Unable to complete backup")?;
    Ok(())
}
