use color_eyre::Result as EyreResult;
use color_eyre::{eyre::WrapErr, Section};
use rbackup_core::config::Config;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

pub fn get_config(config_location: &PathBuf) -> EyreResult<Config> {
    let mut config_file = File::open(config_location)
        .wrap_err(format!(
            "Failed to open configuration file (\"{}\")",
            config_location.display()
        ))
        .suggestion("Make sure that the configuration file exists and has correct permissions")?;
    let mut config_file_contents = String::new();
    config_file
        .read_to_string(&mut config_file_contents)
        .wrap_err(format!(
            "Failed to read configuration file (\"{}\")",
            config_location.display()
        ))
        .suggestion("Make sure that the configuration file has correct permissions")?;

    let config: Config = toml::from_str(&config_file_contents)
        .wrap_err(format!(
            "Failed to parse configuration file (\"{}\")",
            config_location.display(),
        ))
        .suggestion("Make sure you've provided a valid configuration.")?;

    Ok(config)
}

pub fn get_config_location() -> PathBuf {
    const CONFIG: &str = "rbackup/config.toml";

    // Get the base directory for configuration files
    let config_dir = dirs::config_dir().unwrap_or_else(|| {
        let home = dirs::home_dir().expect("Failed to determine user's home directory");
        home.join(".config")
    });

    config_dir.join(CONFIG)
}
